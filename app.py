import os
import json

import bot
import requests
from flask import Flask, request

URL = "https://api.groupme.com/v3/bots/post"

app = Flask(__name__)

# Called when a member posts in the group
@app.route('/', methods=['POST'])
def webhook():
    contents = request.get_json()
    print(contents)
    if contents['name'] != "Turing":
        send_message(contents)
    return "ok", 200


# Have the bot send a message
def send_message(contents):
    text = bot.parse_message(contents)
    if contents['group_id'] == os.getenv("TEST_GROUP_ID"):
        ID = os.getenv("TEST_BOT_ID")
    elif contents['group_id'] == os.getenv("EMSYS_GROUP_ID"):
        ID = os.getenv("EMSYS_BOT_ID")
    params = {'bot_id': ID, 'text': text}
    requests.post(URL, params=params)
