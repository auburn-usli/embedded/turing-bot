# Bot commands, parse arguments and respond to users
import re


def parse_message(contents: dict):
    text = contents['text']
    sender = contents['name']
    attachments = contents['attachments']
    if '69' in text:
        return "Nice."
    elif '/s' in text:
        return sarcasm(text.replace('/s', ''))
    elif is_greeting(text):
        return f"Hi, {sender.split(' ')[0]}."
    # elif attachments:
    #     return "THIS BOY GOT ATTACHMENTS" # debugging message


# is someone saying hi to Turing?
def is_greeting(text: str):
    text = text.lower()
    print(text)
    val = re.search(r"\b(hi|hello|howdy)\b[ ,]+turing", text)
    print(val)
    return val


# sarcasmize a string
def sarcasm(text: str):
    sarcasm_str = ""
    capitalize = text[0].islower()
    for c in text:
        if c.isalpha():
            if capitalize:
                sarcasm_str += c.lower()
                capitalize = False
            else:
                sarcasm_str += c.upper()
                capitalize = True
        else:
            sarcasm_str += c
    return sarcasm_str
